﻿using UnityEngine;
using System.Collections;

public class KillPlayer : MonoBehaviour {

	public LevelManager levelManager;
	public AudioClip die;

	void Start () {
		levelManager = FindObjectOfType<LevelManager>();
	}

	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D objectPlayerCollidedWith) {

		if(objectPlayerCollidedWith.name == "Mario") 
		{
			levelManager.RespawnPlayer();
			SoundController.Instance.PlaySingle(die);
		} 
	}
}
