﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public GameObject startPosition;

	private Player player;

	void Start () {
		player = FindObjectOfType<Player>();
	}

	void Update () {
	
	}

	public void RespawnPlayer () {

		player.transform.position = startPosition.transform.position;
	}
}
