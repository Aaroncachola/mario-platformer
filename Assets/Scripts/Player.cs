﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	public int moveSpeed;
	public int jumpHeight;
	public Transform groundCheck;
	public LayerMask whatIsGround;
	public float groundCheckRadius;
	public AudioClip jump;
	public AudioClip done;
	public Text healthText;

	private float moveVelocity;
	private bool grounded;
	private Animator animator;
	private bool doubleJumped;
	private LayerMask collisionLayer;
	private int playerHealth = 2;
	private int attackPower = 1;
	private int playerLives;

	void Start () {
		collisionLayer = LayerMask.GetMask ("Collision Layer");
		GetComponent<Rigidbody2D>().freezeRotation = true;
		animator = GetComponent<Animator>();
		healthText.text = "Health: " + playerHealth;
	}

	void FixedUpdate () {
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
		animator.SetBool("onGround", grounded);
	}

	void Update () {

		moveVelocity = 0f;

		if(Input.GetKeyDown (KeyCode.UpArrow) || Input.GetKeyDown (KeyCode.W)) 
		{

			if(grounded)
			{
				Jump();
				doubleJumped = false;
				SoundController.Instance.PlaySingle(jump);
			}
			else if(!grounded && !doubleJumped)
			{
				Jump();
				doubleJumped = true;
				SoundController.Instance.PlaySingle(jump);
			}

		}

		else if(Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) 
		{
			moveVelocity = moveSpeed;
			animator.SetTrigger("playerWalk");
		}

		else if(Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) 
		{
			moveVelocity = -moveSpeed;
			animator.SetTrigger("playerWalk");
		} 

		else if(Input.GetKey (KeyCode.RightShift) || Input.GetKey (KeyCode.Space))
		{
			animator.SetTrigger("playerAttack");
		}

		else 
		{
			animator.SetTrigger("playerIdle");
		}

		GetComponent<Rigidbody2D>().velocity = new Vector2(moveVelocity, GetComponent<Rigidbody2D>().velocity.y);

		if(GetComponent<Rigidbody2D>().velocity.x > 0) 
		{
			transform.localScale = new Vector3(2.5f, 2.5f, 1f);
		}
		else if(GetComponent<Rigidbody2D>().velocity.x < 0) 
		{
			transform.localScale = new Vector3(-2.5f, 2.5f, 1f);
		}
	}

	public void Jump() {
		GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
	}

	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith) {
		if (objectPlayerCollidedWith.tag == "Exit") {
			SoundController.Instance.music.Stop();
			SoundController.Instance.PlaySingle(done);
		}
      

      
	}

    
}
