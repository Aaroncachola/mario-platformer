﻿using UnityEngine;
using System.Collections;

public class EnemyPatrol : MonoBehaviour {

	public float moveSpeed;
	public bool moveRight;
	public Transform wallCheck;
	public LayerMask whatIsWall;
	public float wallCheckRadius;
	public Transform edgeCheck;
	public int attackDamage;

	private Animator animator;
	private bool hittingWall;
	private bool notAtEdge;

	void Start () {
		GetComponent<Rigidbody2D>().freezeRotation = true;
		animator = GetComponent<Animator>();
	}
	
	void Update () {

		hittingWall = Physics2D.OverlapCircle (wallCheck.position, wallCheckRadius, whatIsWall);

		notAtEdge = Physics2D.OverlapCircle (edgeCheck.position, wallCheckRadius, whatIsWall);

		if (hittingWall || !notAtEdge) 
		{
			moveRight = !moveRight;
		}

		if(moveRight) 
		{
			GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
			animator.SetTrigger("enemyWalk");
		}
		else 
		{
			GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
			animator.SetTrigger("enemyWalk");
		}

		if(GetComponent<Rigidbody2D>().velocity.x > 0) 
		{
			transform.localScale = new Vector3(1.5f, 1.5f, 1f);
		}
		else if(GetComponent<Rigidbody2D>().velocity.x < 0) 
		{
			transform.localScale = new Vector3(-1.5f, 1.5f, 1f);
		}
	}
}
